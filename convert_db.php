<?php
/*
 * Start like this:
 * MODE-1 - converts a .sql-File
 * php convert_db.php fotolina_01.sql 
 * 
 * MODE-2 - dumps, converts and restore
 * $ php convert_db.php fotolina_01 fotolina01 <passwort>
 */

class ConvertJob {

	public function __construct($argv) {
		if (!isset($argv[1]) || ($argv[1] == "help")) {
			echo "Bitte starten Sie dieses Script mit einem Parameter, der den Pfad zu einer zu konvertierenden .sql-Datei angibt.";
			echo "\n e.g. php /var/www/default/tools/unicode-converter-by-sb/convert_db.php DATABASE USER PASSWORD \n";
			die();
//		} elseif (!file_exists($argv[0])) {
//			echo "Der angegebene Pfad ist ungültig!";
//			die();
		}
//		var_dump($argv);
		# Mode-1 converts an X.sql-file
		if (strstr($argv[1], ".sql")){
			$this->mode = 1;
			$this->sqlFile = $argv[1];
			$this->convertedFile = str_replace(".sql", ".utf8.sql", $this->sqlFile);
		}
		# Mode-2 dump db, converts the X.sql-file, restore db
		elseif (!empty($argv[1]) && !empty($argv[2]) && !empty($argv[3]) ){
			$this->mode = 2;
			$this->sqlFile = "./".$argv[1].".sql";
			$this->convertedFile = str_replace(".sql", ".utf8.sql", $this->sqlFile);
			$this->db = $argv[1];
			$this->user = $argv[2];
			$this->pw = $argv[3];
		}
		
	}

	public function start() {
		if ($this->mode == 2){
			$this->dump($this->db, $this->user, $this->pw);
			echo "\nDump erstellt! \n";
		}
		if ($this->mode == 1 || $this->mode == 2){
			$this->convertSqlFile();
			echo "\n.sql-File konvertiert! \n";
		}
		if ($this->mode == 2){
			$this->db_restore($this->db, $this->user, $this->pw);
			echo "\n.sql-File zurück in die Datenbank gespielt! \n";
		}
		
	}
	public function convertSqlFile(){
		if (!file_exists($this->sqlFile))
			die("sqlFile is missing! \n");
		if (!file_exists($this->convertedFile))
			die("convertedFile is missing! \n");

		$handleIn = fopen($this->sqlFile, "r");
		$handleOut = fopen($this->convertedFile, "w+");
		if (file_exists($this->convertedFile) && $handleOut) {
			while (	($line = fgets($handleIn)) !== false){
				$string = $this->replace($line);
				fwrite($handleOut, $string);
			}
			if (!feof($handleIn)) {
				echo "Error: unexpected fgets() fail\n";
			}
//			fread($handle, $length);
		}
		else {
			die("Konvertierte Datei konnte nicht gelesen werden: ". $this->convertedFile."\n");
		}
		fclose($handleIn);
		fclose($handleOut);
	}

	public function replace($text) {
		$replace = "DEFAULT CHARSET=latin1;";
		$to = "DEFAULT CHARSET=utf8;";
		$text = str_replace($replace, $to, $text);
		$replace = "COLLATE latin1_general_ci";
		$to = "COLLATE utf8_general_ci";
		$text = str_replace($replace, $to, $text);
		$replace = "CHARSET=latin1";
		$to = "CHARSET=utf8";
		$text = str_replace($replace, $to, $text);
		$replace = "COLLATE=latin1_general_ci";
		$to = "COLLATE=utf8_general_ci";
		$text = str_replace($replace, $to, $text);
		$replace = "CHARACTER SET latin1";
		$to = "CHARACTER SET utf8";
		$text = str_replace($replace, $to, $text);
		return $text;
	}
	
	public function dump($db, $user, $pw){
//		return system("mysqldump -u $user -p'$pw' $db > ~/$db.sql");
		$command = "mysqldump -u $user -p'$pw' $db > $this->sqlFile";
		echo "\n$command";
		return system($command);
	}
	public function db_restore($db, $user, $pw){
//		return system("mysqldump -u $user -p'$pw' $db > ~/$db.sql");
		$command = "mysql -u'$user' -p'$pw' $db < $this->convertedFile";
		echo "\n$command";
		return system($command);
		
	}

	/**
	 * String Path to sql-File to convert
	 */
	public $sqlFile;
	/**
	 * String Path to sql-File to convert
	 */
	public $convertedFile;

}
//var_dump($argv);
//var_dump(ini_get("register-argc-argv"));
//echo "äöüß";
$job = new ConvertJob($argv);
$job->start();
echo "\n";
