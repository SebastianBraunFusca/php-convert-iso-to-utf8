<?php

/*
 * Start like this:
 * php convert_files.php /absolute/path/without/a/slash/at/the/end
 */

class ConvertJob {

	public function __construct($argv) {
		if (!isset($argv[1]) || ($argv[1] == "help")) {
			echo "Bitte starten Sie dieses Script mit einem Parameter, der den Pfad zu den zu konvertierenden Dateien angibt.\n";
			echo "e.g. php /var/www/default/tools/unicode-converter-by-sb/convert_files.php /var/www/default/customer/kaffeesolo.de/  \n";
			echo "e.g. php /var/www/default/tools/unicode-converter-by-sb/convert_files.php /var/www/default/customer/kaffeesolo.de/ noreplace \n";
			echo "e.g. php /var/www/default/tools/unicode-converter-by-sb/convert_files.php /var/www/default/customer/kaffeesolo.de/ replace \n";
			die();
//		} elseif (!file_exists($argv[0])) {
//			echo "Der angegebene Pfad ist ungültig!";
//			die();
		}
//		var_dump($argv);
//		echo substr($argv[1], -1)."\n;
		/* Wenn letztes Zeichen ein / ist, dieses trennen! */ 
		if (substr($argv[1], -1) == "/")
//			echo substr($argv[1], 0, strlen($argv[1])-1)."\n";
			$this->sourceFolder = substr($argv[1], 0, strlen($argv[1])-1);
		else
			$this->sourceFolder = $argv[1];
		$this->tmpFolder = $this->sourceFolder . ".utf8";
		$this->bakFolder = $this->sourceFolder . ".iso";
		if (isset($argv[2]) && ($argv[2] == 'replace' || $argv[2] == 'inplace'))
			$this->replace = TRUE;
		elseif (isset($argv[2]) && ($argv[2] == 'noreplace'))
			$this->replace = FALSE;
		else
			$this->replace = FALSE;
		$this->fileTypesToConvert = array(".*", "*.php", "*.htm", "*.html", "*.tpl", "*.css", "*.js", "*.txt", "*.ini", "*.xml","*.svg");
	}

	public function start() {
//		if ($this->mode == 2){
//			$this->dump($this->db, $this->user, $this->pw);
//			echo "\nDump erstellt! ";
//		}
//		if ($this->mode == 1 || $this->mode == 2){
//			$this->convertSqlFile();
//			echo "\n.sql-File konvertiert! ";
//		}
//		if ($this->mode == 2){
//			$this->db_restore($this->db, $this->user, $this->pw);
//			echo "\n.sql-File zurück in die Datenbank gespielt! ";
//		}
//		$command = "rsync -a --iconv=iso88591,utf8 $this->sourceFolder/ $this->tmpFolder";
		$command = "rsync -a $this->sourceFolder/ $this->tmpFolder";
//		var_dump($command);die();
		system($command);
//		$start = getcwd();
		if (!file_exists($this->sourceFolder))
			die("SourceFolder is not valid!\n");
		$commandGoToSrc = "cd $this->sourceFolder";
//		system($commandGoToSrc);#useless but for test
		echo "Source Folder: ";
		$goToSrcTest = system("$commandGoToSrc && pwd",$goToSrcTest);
//		system("$commandGoToSrc && pwd",$goToSrcTest);
//		var_dump($goToSrcTest);
//		var_dump($command);
//		var_dump(system("pwd"));
//		var_dump(getcwd());
////		var_dump(system("cd $this->sourceFolder && pwd"));
//		var_dump(system("pwd"));
//		var_dump(getcwd());
//		die();
//		var_dump($this->sourceFolder);
		if (!($goToSrcTest == $this->sourceFolder))
			die("SourceFolder is not valid directory!\n");

		foreach ($this->fileTypesToConvert as $type) {
			$command = "$commandGoToSrc && find . -type f -name '$type' -exec iconv -f ISO-8859-15 -t UTF-8 {} -o $this->tmpFolder/{} \;";
//			var_dump($command);die();
			system($command);
		}
		if ($this->replace)
			$this->replace();
		
	}

	public function replace() {
		$command = "mv $this->sourceFolder  $this->bakFolder";
		system($command);
		$command = "mv $this->tmpFolder  $this->sourceFolder";
		system($command);
	}

//	public function convertSqlFile(){
//		$handleIn = fopen($this->sqlFile, "r");
//		$handleOut = fopen($this->convertedFile, "w+");
//		if (file_exists($this->convertedFile) && $handleOut) {
//			while (	($line = fgets($handleIn)) !== false){
//				$string = $this->replace($line);
//				fwrite($handleOut, $string);
//			}
//			if (!feof($handleIn)) {
//				echo "Error: unexpected fgets() fail\n";
//			}
////			fread($handle, $length);
//		}
//		fclose($handleIn);
//		fclose($handleOut);
//	}
//	public function replace($text) {
//		$replace = "DEFAULT CHARSET=latin1;";
//		$to = "DEFAULT CHARSET=utf8;";
//		$text = str_replace($replace, $to, $text);
//		$replace = "COLLATE latin1_general_ci";
//		$to = "COLLATE utf8_general_ci";
//		$text = str_replace($replace, $to, $text);
//		$replace = "CHARSET=latin1";
//		$to = "CHARSET=utf8";
//		$text = str_replace($replace, $to, $text);
//		$replace = "COLLATE=latin1_general_ci";
//		$to = "COLLATE=utf8_general_ci";
//		$text = str_replace($replace, $to, $text);
//		$replace = "CHARACTER SET latin1";
//		$to = "CHARACTER SET utf8";
//		$text = str_replace($replace, $to, $text);
//		return $text;
//	}
//	public function dump($db, $user, $pw){
////		return system("mysqldump -u $user -p'$pw' $db > ~/$db.sql");
//		$command = "mysqldump -u $user -p'$pw' $db > $this->sqlFile";
//		echo "\n$command";
//		return system($command);
//	}
//	public function db_restore($db, $user, $pw){
////		return system("mysqldump -u $user -p'$pw' $db > ~/$db.sql");
//		$command = "mysql -u'$user' -p'$pw' $db < $this->convertedFile";
//		echo "\n$command";
//		return system($command);
//		
//	}
//	/**
//	 * String Path to sql-File to convert
//	 */
//	public $sqlFile;
}

//var_dump($argv);
//var_dump(ini_get("register-argc-argv"));
//echo "äöüß";
$job = new ConvertJob($argv);
$job->start();
echo "\n";
